﻿namespace ConsoleEShop_Level.Services
{
    public interface IJsonService
    {
        T Deserialize<T>(string json);
        string Serialize<T>(T obj);
    }
}
