﻿namespace ConsoleEShop_Level.Views
{
    public interface IView
    {
        void ShowMessage(string message);
        int ShowMainMenu();
        (string, string) ShowLogin();
        string ShowOldPassword();
        string ShowNewPassword();
        (string, string) ShowRegistration();
        int ShowGuestOptions();
        int ShowUserById();
        int ShowProductById();
        string ShowProductByName();
        (string, string, string, decimal) ShowProduct();
        (string, string, string, decimal) ShowChangedProduct();
        int ShowOrderStatusAdmin();
        int ShowOrderId();
        int ShowUserOptions();
        int ShowAdminOptions();

    }
}
