﻿using System.Collections.Generic;
using ConsoleEShop_Level.Models;

namespace ConsoleEShop_Level.Repositories
{
    public interface IUsersRepository : IRepository<User>
    {
        User GetUserByLogin(string login);
        User GetUserByLoginAndPassword(string login, string password);
    }
}
