﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ConsoleEShop_Level.Models;
using ConsoleEShop_Level.Services;
using AppContext = ConsoleEShop_Level.Models.AppContext;

namespace ConsoleEShop_Level.Repositories
{
    public class OrdersRepository: IOrdersRepository
    {
        private readonly IDictionary<int, Order> _orders;
        private readonly IFileService _fileService;
        private readonly IJsonService _jsonService;

        public OrdersRepository(AppContext context, ServiceManager serviceManager)
        {
            _fileService = serviceManager.FileService;
            _jsonService = serviceManager.JsonService;
            _orders = context.Orders;
        }

        public void Load(string fileName)
        {
            var orders = _jsonService.Deserialize<IEnumerable<Order>>(_fileService.Read(fileName)) ?? new List<Order>();

            foreach (var order in orders)
                Save(order);
        }

        public void Write(string fileName) => FileService.Write(fileName, _jsonService.Serialize(_orders.Values));

        public IDictionary<int, Order> GetAll() => _orders;
        public Order GetById(int id)
        {
            var order = _orders[id];
            if (order == null) throw new KeyNotFoundException("Order with such Id does not exist.");
            return order;
        }
        public Order GetOrderByUserId(int userId) => _orders.Values.FirstOrDefault(order => order.UserId == userId);
        public List<Order> GetOrdersByUserId(int userId) => _orders.Values.Where(order => order.UserId == userId).ToList();

        public void Save(Order order, bool isWrite = true)
        {

            if (_orders.ContainsKey(order.Id))
            {
                _orders[order.Id] = order;
                if(isWrite) Write("orders.json");
                return;
            }

            _orders.Add(order.Id, order);
            if(isWrite) Write("orders.json");

        }

    }
}
